import React, { useEffect } from 'react'
import CountryCard from './CountryCard'
import SortComponent from './SortComponent';
import { useState } from 'react';
import { useContext } from 'react';
import { DarkModeContext } from './DarkModeContext';
import NavBar from './NavBar';
import SearchComponent from './SearchComponent';
import Filter from './Filter';
// Add catch block and loading spinner
const Homepage = () => {
    const [searchQuery, setSearchQuery] = useState('');
    const [selectedRegion, setSelectedRegion] = useState('');
    const [countryData, setCountryData] = useState([]);
    const [selectedSubRegion, setSelectedSubRegion] = useState('');
    const [sortPopulationFilterChange, setSortPopulationFilterChange] = useState('');
    const [sortAreaFilterChange, setSortAreaFilterChange] = useState('');
    const [region,setRegion]=useState([]);
    const [subRegionOfSelectedRegion, setSubRegionOfSelectedRegion] = useState([]);
    const { darkMode, handleDarkModeChange } = useContext(DarkModeContext);

    const handleChange = (event) => {
        setSearchQuery(event.target.value);
        console.log(searchQuery);
    }

    const handleFilterChange = (event) => {
        let selectedValue= event.target.value;
        console.log(selectedValue);
        setSelectedRegion(selectedValue);
        setSelectedSubRegion('');

        if (selectedValue !== '') {
            const subRegions = countryData
                .filter((country) => country.region === selectedValue)
                .map((country) => country.subregion);
    
            const uniqueSubRegions = subRegions.filter((subregion, index) => {
                return subRegions.indexOf(subregion) === index && subregion !== null;
            });
    
            setSubRegionOfSelectedRegion(uniqueSubRegions);
        } else {
            setSubRegionOfSelectedRegion([]);
        }
    
    }

    const handleSortPopulationFilterChange = (event) => {
        setSortPopulationFilterChange(event.target.value);
        console.log(event.target.value);
    }

    const handleSortAreaFilterChange = (event) => {
        setSortAreaFilterChange(event.target.value);
        console.log(event.target.valueturn);
    }

    const getRegions = () => {
        const regionSet = new Set(); //set automatically will remove duplicates
        countryData.forEach((country) => {
            regionSet.add(country.region);
        });
        setRegion(Array.from(regionSet)); //Converting set to array using array.from
        return Array.from(regionSet)
    };

    // const region = getRegions()

    // const getRegions = () => {
    //     let r = countryData.map((country) => country.region);
    //     setRegion(r.filter((region, index) => {
    //         return (r.indexOf(region) == index);
    //     }));
    // }

    // useEffect(() => {
    //     let subRegions = countryData.filter((country) => country.region == selectedRegion).map((country) => country.subregion);//Only the selected region counties sunregion must be displayed
    //     setSubRegionOfSelectedRegion(subRegions.filter((subregion, index) => {
    //         return (subRegions.indexOf(subregion) == index) && (subregion != null) //if first index===current index, so it will add it to array else that means the index is not first index so data is duplicate so it will remove it
    //     }))
    // }, [selectedRegion])

    const handleSubRegionFilterChange = (event) => {
        setSelectedSubRegion(event.target.value);
    }

    useEffect(() => {
        fetch('https://restcountries.com/v3.1/all')
            .then((data) => data.json())
            .then((data) => {
                setCountryData(data);
                getRegions();
            })
            .catch((error)=>console.log(error));        
    }, [])

    console.log(region);

    return (
        <div className={darkMode ? 'dark' : ''}>
            <NavBar />

            <div className='bg-gray-50  dark:bg-customColor'>

                <div className='flex justify-around'>

                    <SearchComponent onChange={handleChange}/>

                    <SortComponent value="population" onChange={handleSortPopulationFilterChange}/>

                    <SortComponent value="area" onChange={handleSortAreaFilterChange}/>

                    <Filter
                        label="Filter by subregion"
                        options={subRegionOfSelectedRegion}
                        value={selectedSubRegion}
                        onChange={handleSubRegionFilterChange}
                    />

                    <Filter
                        label="Filter by region"
                        options={region}
                        value={selectedRegion}
                        onChange={handleFilterChange}
                        onClickHandler={getRegions}
                    />

                    {/* <select className=' my-8 px-4 py-2 mr-2 bg-white border-white outline-none shadow-lg dark:bg-customColor1 dark:text-white ' onChange={handleSubRegionFilterChange}>
                        <option value="" disabled selected>Filter by subregion</option>
                        {subRegionOfSelectedRegion.map((subregion, index) => (
                            <option value={subregion} key={index}>{subregion}</option>
                        ))}
                    </select>

                    <select className='mr-32 my-8 px-4 py-2 bg-white border-white outline-none shadow-lg dark:bg-customColor1 dark:text-white ' onChange={handleFilterChange}  onClick={getRegions}>
                        <option value="" disabled selected >Filter by region</option>
                        {countryData.length>0 && region.map((region)=>{
                            return <option value={region}>{region}</option>
                        })}
                    </select> */}
                </div>
            </div>

            <div>
                <CountryCard searchQuery={searchQuery} selectedRegion={selectedRegion} selectedSubRegion={selectedSubRegion} sortPopulationFilter={sortPopulationFilterChange} sortAreaFilter={sortAreaFilterChange} countries={countryData}/>
            </div>
        </div>

    )
}

export default Homepage