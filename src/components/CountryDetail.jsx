import React, { useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { useEffect } from 'react'
import { DarkModeContext } from './DarkModeContext';
import { useContext } from 'react';

const CountryDetail = ({ countries }) => {
    const params = useParams()
    const id = params.id;
    const navigate = useNavigate();
    let [countryDetail, setCountryDetail] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const { darkMode, handleDarkModeChange } = useContext(DarkModeContext);
    const [borderCountries, setBorderCountries] = useState(null)


    const countryCodeToName = {};
  
    countries.forEach((country) => {
        countryCodeToName[country.cca3] = country.name.official;
    });


    const getCurrencies = (countryDetail) => {

        if (countryDetail && countryDetail.currencies) {
            const key = Object.keys(countryDetail.currencies)[0];
            console.log(countryDetail.currencies[key].name);
            return countryDetail.currencies[key].name;
        }
        return null;
    }

    const getNativeName = (countryDetail) => {

        if (countryDetail && countryDetail.name && countryDetail.name.nativeName) {
            const key = Object.keys(countryDetail.name.nativeName)[0];
            console.log(countryDetail.name.nativeName[key].official);
            return countryDetail.name.nativeName[key].official;
        }
        return null;
    }

    const getLanguages = (countryDetail) => {

        if (countryDetail && countryDetail.currencies) {
            const languages = Object.values(countryDetail.languages).join(',');
            console.log(languages);
            return languages;
        }
        return null;
    }

    // const getBorder = (countryDetail) => {

    //     if (countryDetail && countryDetail.borders) {
    //         const borders = countryDetail.borders;
    //         console.log(borders);
    //         return borders;
    //     }
    //     return null;
    // }

    // const getBorderNames = (countryDetail) => {
    //     let borderCodes = getBorder(countryDetail);
    //     if (borderCodes) {
    //         const promises = borderCodes.map((code) => {
    //             return fetch(`https://restcountries.com/v3.1/alpha/${code}`)
    //                 .then((response) => response.json())
    //                 .then((data) => data[0].name.official)
    //                 .catch((error) => console.log(error))
    //         })

    //         Promise.all(promises)
    //             .then((borderNames) => {
    //                 console.log(borderNames)
    //                 setBorderCountries(borderNames)
    //             })
    //             .catch((error) => console.log(error));
    //     }
    //     else {
    //         setBorderCountries([]);
    //     }
    // }

    useEffect(() => {
        fetch(`https://restcountries.com/v3.1/alpha/${id}`)
            .then((data) => data.json())
            .then((data) => {
                setCountryDetail(data[0]);
                console.log(data[0]);
                setIsLoading(false);
                getBorderNames(data[0]);
            })
            .catch((error) => console.log(error));
    }, [id])




    return (
        <>
            {isLoading ? (
                <div>
                    <div className="text-center">
                        <div className="animate-spin rounded-full h-10 w-10 border-gray-900 border-t-2 border-b-2 mx-auto dark:border-white mt-10"></div>
                        <p className="text-2xl mt-4 dark:text-white">Loading...</p>
                    </div>
                </div>
            ) : (
                <>
                    <div className={darkMode ? 'dark' : ''}>
                        <div className='dark:bg-customColor dark:text-white min-h-screen overflow-hidden'>
                            <button onClick={() => { navigate(-1) }} className='mt-10 mx-24 px-8 py-4 shadow-md shadow-gray-500 border-white outline-none w-32 flex justify-between text-center dark:bg-customColor1'>
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="size-6">
                                    <path strokeLinecap="round" strokeLinejoin="round" d="M10.5 19.5 3 12m0 0 7.5-7.5M3 12h18" />
                                </svg> Back </button>
                            <div className='container flex my-20 ml-24 '>
                                {countryDetail && <img src={countryDetail.flags.png} className='display-image' />}
                                <div className='details'>
                                    <h2 className='text-3xl font-bold mb-5'>{countryDetail && countryDetail.name.common}</h2>
                                    <div className='inner-container flex justify-between'>
                                        <div className='left-details'>
                                            <div className='mb-2'>
                                                <label className='font-bold '>Native name: </label>
                                                <span>{getNativeName(countryDetail)}</span>
                                            </div>
                                            <div className='mb-2'>
                                                <label className='font-bold'>Population: </label>
                                                <span>{countryDetail.population}</span>
                                            </div>
                                            <div className='mb-2'>
                                                <label className='font-bold'>Region: </label>
                                                <span>{countryDetail.region}</span>
                                            </div>
                                            <div className='mb-2'>
                                                <label className='font-bold'>Sub Region: </label>
                                                <span>{countryDetail.subregion}</span>
                                            </div>
                                            <div className='mb-2'>
                                                <label className='font-bold'>Capital: </label>
                                                <span>{countryDetail.capital}</span><br />
                                            </div>

                                        </div>
                                        <div className='right-details'>
                                            <div className='mb-2'>
                                                <label className='font-bold'>Top Level Domain: </label>
                                                <span>{countryDetail.tld}</span>
                                            </div>
                                            <div className='mb-2'>
                                                <label className='font-bold'>Currencies: </label>
                                                <span>{getCurrencies(countryDetail)}</span>
                                            </div>
                                            <div className='mb-2'>
                                                <label className='font-bold'>Languages: </label>
                                                <span>{getLanguages(countryDetail)}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='mt-7'>
                                        <   label className='font-bold'>Border Countries:</label>
                                        {countryDetail && countryDetail.borders && countryDetail.borders.length !== 0 ? (
                                            countryDetail.borders.map((code, index) => (
                                            <span
                                                key={index}
                                                className='bg-white shadow-sm shadow-gray-400 text-black px-3 dark:bg-customColor1 dark:text-white ml-3 mb-3'
                                            >
                                                {countryCodeToName[code]}
                                            </span>
                                            ))
                                        ) : 
                                        (<span> No Border Countries</span>)}


                                        {/* {borderCountries == null ?
                                            (<span> Loading border countries</span>) :
                                            borderCountries.length != 0 ? borderCountries.map((country, index) =>
                                            (<span key={index} className='bg-white shadow-sm shadow-gray-400 text-black px-3 dark:bg-customColor1 dark:text-white ml-3 mb-3 '>
                                                {country} </span>)) : */}
                                               
                                        

                                        {/* <span>{getBorderNames(countryDetail)}</span> */}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </>

            )}

        </>

    )
}

export default CountryDetail