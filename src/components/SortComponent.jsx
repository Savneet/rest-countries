import React from 'react'

const SortComponent = ({value,onChange}) => {
  return (
    <>
      <select className=' my-8 px-4 py-2 mr-2 bg-white border-white outline-none shadow-lg dark:bg-customColor1 dark:text-white ' onChange={onChange}>
            <option value="" disabled selected>Sort by {value}</option>
            <option value="Ascending">Ascending</option>
            <option value="Descending">Descending</option>
        </select>
    </>
  )
}

export default SortComponent