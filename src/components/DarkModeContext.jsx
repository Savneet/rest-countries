import React, { createContext, useState } from 'react';

export const DarkModeContext = createContext();

export const DarkModeProvider = (props) => {
  const [darkMode, setDarkMode] = useState(false);

  const handleDarkModeChange = () => {
    setDarkMode(!darkMode);
  };

  return (
    <DarkModeContext.Provider value={{ darkMode, handleDarkModeChange }}>
      {props.children}
    </DarkModeContext.Provider>
  );
};