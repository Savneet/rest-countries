import React from 'react'
import SearchIcon from '../assets/search-icon.svg'

const SearchComponent = ({onChange}) => {
  return (
    <>
        <div className='flex justify-between w-80 pl-11 py-4 my-8 ml-7  bg-white border-white rounded-md shadow-lg dark:bg-customColor1 dark:text-white'>
            <img src={SearchIcon} alt="search-icon" className='w-4' />
            <div>
                <input type='text' placeholder=' Search for a country...' className='outline-none dark:bg-customColor1 dark:text-white' onChange={onChange}></input>
            </div>
        </div>
    </>
  )
}

export default SearchComponent