import React from 'react'
import { useContext } from 'react';
import { DarkModeContext } from './DarkModeContext';
const NavBar = () => {
    const { darkMode, handleDarkModeChange } = useContext(DarkModeContext);
    return (
        <>
            <div className={darkMode ? 'dark' : ''}>
                <div className='dark:bg-customColor1'>
                    <header className='flex justify-between px-24 py-10'>
                        <h1 className='text-3xl font-bold dark:text-white'>Where in the world?</h1>
                        <div className='flex justify-between'>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="size-6 mt-2 mr-2 dark:text-white dark:fill-white">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M21.752 15.002A9.72 9.72 0 0 1 18 15.75c-5.385 0-9.75-4.365-9.75-9.75 0-1.33.266-2.597.748-3.752A9.753 9.753 0 0 0 3 11.25C3 16.635 7.365 21 12.75 21a9.753 9.753 0 0 0 9.002-5.998Z" />
                            </svg>
                            <button className='text-lg mr-28 dark:text-white' onClick={handleDarkModeChange}>  {darkMode ? 'Light Mode' : 'Dark Mode'}</button>
                        </div>
                    </header>
                </div>
            </div>
            <hr className='border-2 grey'></hr>
        </>
    )
}

export default NavBar