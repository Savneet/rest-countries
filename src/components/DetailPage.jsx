import React from 'react'
import NavBar from './NavBar'
import CountryDetail from './CountryDetail'
import { useLocation } from 'react-router-dom'

const DetailPage = () => {
  const location = useLocation();
  const countries = location.state?.countries || [];
  return (
    <>
        <NavBar/>
        <CountryDetail countries={countries}/>
    </>
  )
}

export default DetailPage