import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
const CountryCard = ({ searchQuery, selectedRegion, selectedSubRegion, sortPopulationFilter, sortAreaFilter, countries }) => {

    const navigate = useNavigate();

    const isLoading = !countries || countries.length === 0;

    const filterByQuery = (countries, searchQuery) => {
        return countries.filter((country) =>
            country.name.common.toLowerCase().includes(searchQuery.toLowerCase())
        );
    };

    const filterByRegion = (countries, selectedRegion) => {
        return countries.filter((country) => country.region === selectedRegion);
    };

    const filterBySubRegion = (countries, selectedSubRegion) => {
        return countries.filter((country) => country.subregion === selectedSubRegion);
    };

    const sortByPopulation = (countries, sortPopulationFilter) => {
        if (sortPopulationFilter === 'Ascending') {
            return [...countries].sort((a, b) => a.population - b.population);
        } else if (sortPopulationFilter === 'Descending') {
            return [...countries].sort((a, b) => b.population - a.population);
        }
        return countries;
    };

    const sortByArea = (countries, sortAreaFilter) => {
        if (sortAreaFilter === 'Ascending') {
            return [...countries].sort((a, b) => a.area - b.area);
        } else if (sortAreaFilter === 'Descending') {
            return [...countries].sort((a, b) => b.area - a.area);
        }
        return countries;
    };

    const applyFiltersAndSorting = (countries, searchQuery, selectedRegion, selectedSubRegion, sortPopulationFilter, sortAreaFilter) => {
        let filtered = countries;

        if (searchQuery) {
            filtered = filterByQuery(filtered, searchQuery);
        }

        if (selectedRegion) {
            filtered = filterByRegion(filtered, selectedRegion);
        }

        if (selectedSubRegion) {
            filtered = filterBySubRegion(filtered, selectedSubRegion);
        }

        filtered = sortByPopulation(filtered, sortPopulationFilter);
        filtered = sortByArea(filtered, sortAreaFilter);


        return filtered;
    };


    const filteredCountries = applyFiltersAndSorting(
        countries,
        searchQuery,
        selectedRegion,
        selectedSubRegion,
        sortPopulationFilter,
        sortAreaFilter
    );


    // useEffect(() => {
    //     let filtered = countries;
    //     if (searchQuery) {
    //         filtered = countries.filter((country) =>
    //             country.name.common.toLowerCase().includes(searchQuery.toLowerCase()));
    //     }

    //     if (selectedRegion) {
    //         filtered = filtered.filter((country) => country.region === selectedRegion);

    //     }

    //     if (selectedSubRegion) {
    //         filtered = filtered.filter((country) => country.subregion === selectedSubRegion);
    //     }


    //     if (sortPopulationFilter === 'Ascending') {
    //         filtered = [...filtered].sort((a, b) => {
    //             return a.population - b.population;
    //         })
    //     }

    //     if (sortPopulationFilter === 'Descending') {
    //         filtered = [...filtered].sort((a, b) => {
    //             return b.population - a.population;
    //         })
    //     }

    //     if (sortAreaFilter) {
    //         filtered = [...filtered].sort((a, b) => {
    //             return sortAreaFilter === 'Ascending' ? (a.area - b.area) : (b.area - a.area);
    //         })
    //     }

    //     setFilteredCountries(filtered);

    //     setNoCountry(filtered.length === 0);
    // }, [searchQuery, selectedRegion, selectedSubRegion, countries, sortPopulationFilter, sortAreaFilter]);

    return (
        <>
            {isLoading ? (
                <div>
                    <div className="text-center">
                        <div className="animate-spin rounded-full h-10 w-10 border-gray-900 border-t-2 border-b-2 mx-auto dark:border-white"></div>
                        <p className="text-2xl mt-4 dark:text-white">Loading...</p>
                    </div>
                </div>
            ) : (
                <div className='min-h-screen dark:bg-customColor bg-gray-50'>
                    <div className='cards grid grid-cols-4 px-24 py-4 gap-y-12 gap-x-8 bg-gray-50 dark:bg-customColor'>
                        {filteredCountries.length==0 ? (
                            <p className="text-center text-2xl col-span-4 dark:text-white">No such countries found</p>
                        ) : (
                            filteredCountries.map((country) => (
                                <div className='w-72 shadow-lg rounded-lg cursor-pointer' key={country.ccn3} onClick={() => { navigate(`/country/${country.ccn3}`, { state: { countries }}) }}>
                                    <img src={country.flags.png} className='w-full h-44 rounded-t-lg' />
                                    <div className='content bg-white pl-3 py-5 rounded-b-lg dark:bg-customColor1 dark:text-white' key={country.ccn3}>
                                        <h5 className='text-xl font-bold mb-2'>{country.name.common}</h5>
                                        <label className='text-sm font-bold'>Population: </label>
                                        <span className='text-sm'>{country.population}</span><br />
                                        <label className='text-sm font-bold'>Region: </label>
                                        <span className='text-sm'>{country.region}</span><br />
                                        <label className='text-sm font-bold'>Capital: </label>
                                        <span className='text-sm'>{country.capital}</span> <br />
                                        <label className='text-sm font-bold'>Subregion: </label>
                                        <span className='text-sm'>{country.subregion}</span> <br />
                                        <label className='text-sm font-bold'>Area: </label>
                                        <span className='text-sm'>{country.area}</span>
                                    </div>
                                </div>
                            ))
                        )}
                    </div>
                </div>
            )}
        </>

    )
}

export default CountryCard