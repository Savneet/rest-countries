import React from 'react';

const Filter = ({
    label,
    options,
    value,
    onChange,
    onClickHandler = null,
}) => {
    return (
        <select
            className="mr-32 my-8 px-4 py-2 bg-white border-white outline-none shadow-lg dark:bg-customColor1 dark:text-white"
            value={value}
            onChange={onChange}
            onClick={onClickHandler}
        >
            <option value="" disabled>
                {label}
            </option>
            {options.map((option, index) => (
                <option key={index} value={option}>
                    {option}
                </option>
            ))}
        </select>
    );
};

export default Filter;