import Homepage from './components/Homepage'
import { DarkModeProvider } from './components/DarkModeContext'
import { Route, Routes } from 'react-router-dom'
import DetailPage from './components/DetailPage'


function App() {

  return (
    <>
    <DarkModeProvider>
      <Routes>
        <Route path='/' element={<Homepage/>}></Route>
        <Route path='/country/:id' element={<DetailPage/>}></Route>
      </Routes>
    </DarkModeProvider>
    </>
  )
}

export default App
